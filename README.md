Aluno: Rodrigo Doria Vilaça

Matricula: 140031111

Universidade de Brasilia

Disciplina: Técnicas de progrmação 1.

-----------------------------------------------------------------------------

# PARTE 1 - BOUNCING BALL

## Introdução

Esta é a primeira parte do projeto da disciplina Técnicas de programação 1, onde faremos nosso primeiro experimento com programação orientada a objetos em C++.

Neste trabalho, usaremos códigos e textos providos pelo seguinte link:

http://www.robots.ox.ac.uk/~victor/teaching/labs/b16/

Sendo o arquivo B16 Lab Notes, um PDF explicativo e introdutório do código desecrevendo algumas tarefas a serem feitas e testadas e os códigos contidos em "B16 Lab Code" são os que usaremos no decorrer deste trabalho.

-----------------------------------------------------------------------------

## Objetivo e arquitetura

O objetivo deste primeiro trabalho é ter uma introdução a programação orientada a objetos em C++ e usaremos o exemplo Bouncing Ball para isso.

O código Bouncing Ball é divido em diversos arquivos, sendo os mais importantes:

-> ball.h - Onde é definida a classe Ball com todos seus atributos e métodos.

-> ball.cpp - Onde são de fato implementados os métodos da classe Ball.

-> test-ball.cpp - Programa que contém a main e fará os testes usando a classe Ball e seus métodos.

-----------------------------------------------------------------------------

## Classes e métodos

O projeto Boucing Ball pode ser abstraído como uma bola quicando. 

A classe ball possui todos atributos necessários desta bola que são: raio, posição (x e y), velocidade, massa, gravidade, e as limitações (coordenadas) da caixa onde está bola estará quicando. 

Os métodos implementados são os seguintes:

Display - Mostra as coordenadas da bola (x e y) na tela.

Step - Atualiza as coordenadas da bola, fazendo o próximo passo que a bola fará, calculando suas coordenadas de acordo com a velocidade, coordenadas e características da bola dadas.

Também temos métodos novos criados como parte do projeto que são setPosition, getPositionX e getPositionY. Estes métodos permitem ao usuário definir e verificar as coordenadas da bola.
Também exitem 2 construtores. O primeiro definindo os atributos da bola ao ser instanciada, e o segundo dando opção para o usuário definir as coordenadas iniciais da bola.

-----------------------------------------------------------------------------

## Compilando

O projeto pode ser compilado através de IDEs como Visual Studio para Windows ou XCode para Mac.

Também pode ser executado no terminal linux através do comando:

`g++ ball.cpp test-ball.cpp -o teste`

Podemos executar o programa criado utilizando o comando "./teste".

-----------------------------------------------------------------------------

## Diagrama de Classes

![Diagrama de Classes](ClassesBall.png)

-----------------------------------------------------------------------------

## Exemplo de saída

Abaixo segue um exemplo de saída do programa (coordenadas x e y mostradas para cada passo do loop):

0.01	-0.00877778

0.02	-0.0284444

0.03	-0.059

0.04	-0.100444

0.05	-0.152778

0.06	-0.216

0.07	-0.290111

0.08	-0.375111

0.09	-0.471

0.1		-0.577778

0.11	-0.695444

0.12	-0.824

(...)

0.86	-0.411

0.87	-0.511111

0.88	-0.622111

0.89	-0.744

0.89	-0.876778

0.88	-0.876778

0.87	-0.744

0.86	-0.622111

0.85	-0.511111

0.84	-0.411

0.83	-0.321778

0.82	-0.243444

0.81	-0.176

0.8		-0.119444

0.79	-0.0737778

-----------------------------------------------------------------------------

## Grafico das coordenadas

Por fim segue um gráfico das coordenadas mostradas acima:

![Grafico da saída](GraphBall.jpg)

-----------------------------------------------------------------------------

# PARTE 2 - SPRING MASS

## Introdução

Nesta parte do trabalho, iremos trabalhar com o projeto spring mass. Agora a complexidade está maior, uma vez que não só trabalharemos com uma classe bola, mas sim com classes de massas, molas, massas-molas, e a interação entre elas. Também usaremos uma classe extra ja implementada chamada Vector2, que nos ajudará nos cálculos, quando mexermos com as variáveis armazenadas nessa classe, como posição, velocidade e força.

-----------------------------------------------------------------------------

## Arquitetura

Este projeto é divido em 3 arquivos:

-> springmass.h - Onde são definidas as classes Vector2, Mass, Spring, SpringMass com todos seus atributos e métodos.

-> springmass.cpp - Onde são de fato implementados os métodos das classes definidas acima.

-> test-springmass.cpp - Programa que contém a main e fará os testes usando as classes e seus métodos.

-----------------------------------------------------------------------------

## Classes e Métodos

### Classe Vector2

Esta classe armazena dois valores double: x e y, e poderemos usá-la para armazenar posição, velocidade ou força.

Muitas contas que precisarão ser feitas com essas variáveis poderão ser simplificadas usando esta classe, pois além de métodos úteis como norm, que retorna o valor double do vetor resultante dessas variáveis, também possui sobrecarregação de vários operadores para facilitar tais contas.

### Classe Mass

Possui atributos necessários da massa: Posição, Velocidade, massa, raio, força, e as limitações de espaço.

Também possui os seguintes métodos:

SetForce, addForce, getForce, getPosition, getVelocity, getMass, getRadius, getEnergy e step, este último atualizando os atributos da bola a cada passo executado.

### Classe Spring

Para "construir" a mola, necessitamos dos seguintes atributos: Duas massas (da classe Mass), que são as extremidades da mola, e as variáveis naturalLength (comprimento), stiff (coeficiente elastico da mola) e damping (coeficiente de amortecimento).

Dentre seus métodos, temos:

getMass1, getMass2, getForce, getLenght, getEnergy.

Também foi definido uma sobrecarga no operador >>, permitindo a impressão das coordenadas mais facilmente.

### Classe SpringMass

Contem atributos: Gravidade, e vetores de massas e molas.

Metodos: step, display, getEnergy, addMass, addSpring


-----------------------------------------------------------------------------

## Compilando

O projeto pode ser compilado através de IDEs como Visual Studio para Windows ou XCode para Mac.

Também pode ser executado no terminal linux através do comando:

`g++ springmass.cpp test-springmass.cpp -o teste2`

Podemos executar o programa criado utilizando o comando `./teste2`

-----------------------------------------------------------------------------

## Diagrama de Classes

![Diagrama de Classes](ClassesSpring.png)

-----------------------------------------------------------------------------

## Exemplo de saida:

(coordenadas massa 1) , (coordenadas massa 2) , $comprimentoMola

(-0.499444,-0.0009) , (0.499444,-0.0009) , $0.998889

(-0.497798,-0.0036) , (0.497798,-0.0036) , $0.995595

(-0.495123,-0.0081) , (0.495123,-0.0081) , $0.990246

(-0.49153,-0.0144) , (0.49153,-0.0144) , $0.98306

(-0.48717,-0.0225) , (0.48717,-0.0225) , $0.974341

(-0.482231,-0.0324) , (0.482231,-0.0324) , $0.964462

(-0.476927,-0.0441) , (0.476927,-0.0441) , $0.953854

(-0.47149,-0.0576) , (0.47149,-0.0576) , $0.942979

(-0.46616,-0.0729) , (0.46616,-0.0729) , $0.93232

(-0.461176,-0.09) , (0.461176,-0.09) , $0.922352

(-0.456762,-0.1089) , (0.456762,-0.1089) , $0.913524

(-0.453119,-0.1296) , (0.453119,-0.1296) , $0.906239

(-0.450417,-0.1521) , (0.450417,-0.1521) , $0.900834

(...)

PS: Também é possível imprimir energia do total do sistema, mas esta parte está comentada.

-----------------------------------------------------------------------------

## Grafico das coordenadas

Por fim segue um gráfico das coordenadas mostradas acima:

![Grafico da saída](GraphicSpring.jpg)

-----------------------------------------------------------------------------

# PARTE 3 - GRAPHICS

## Introdução

Nesta terceira parte do projeto, nos concentraremos na área de conhecimentos gráficos.

Utilizaremos a biblioteca as bibliotecas Glut e OpenGL para gerar gráficos das duas partes anteriores do projeto.

A parte gráfica da primeira parte do projeto (Bouncing Ball) já está implementada, sendo nosso objetivo entender como as biliotecas gráficas funcionam, como compilar, como as classes de "desenho" são declaradas e implementadas, etc.

O real desafio desta terceira parte será adaptar o código para que a segunda parte do projeto (Spring Mass) também seja vizualizada graficamente.

-----------------------------------------------------------------------------

## Arquitetura

Esta parte do projeto é divida em 4 arquivos:

-> graphics.h - Onde são definidas as classes Drawable e Figure com todos seus atributos e métodos.

-> graphics.cpp - Onde são de fato implementados os métodos das classes definidas acima.

-> test-ball-graphics.cpp - Programa que contém a main e fará os testes usando as classes acima assim como os arquivos da parte 1 (Bouncing Ball) para gerar a bouncing ball graficamente.

-> test-springmass-graphics.cpp - Programa que contém a main e fará os testes usando as classes acima e seus métodos, assim  como os arquivos da parte 2 (Spring Mass) para gerar Spring Mass graficamente.

-----------------------------------------------------------------------------

## Classes e Métodos

### Classe Drawable

Esta classe contém apenas o método draw() que será usada para conseguirmos "desenhar" graficamente nossos objetos.

### Classe Figure

Possui atributos necessários para gerar uma janela com uma figura: id, Largura da janela, Altura da janela, tamanho do pixel, as limitações de espaço, vetor de objetos, e varíaveis do tipo GLuint que geram círculo e grade na figura.

Também possui os seguintes métodos:

AddDrawable, removeDrawables, update, reshape, draw, drawString, drawCircle, drawLine, handleDisplay, handleReshape, findByWindowId, updateGrid, makeCurrent.

-----------------------------------------------------------------------------

## Compilando

O projeto pode ser compilado através de IDEs como Visual Studio para Windows ou XCode para Mac.

Também pode ser executado no terminal linux através do comando (para mac):

`g++ test-ball-graphics.cpp graphics.cpp ball.cpp -framework GLUT -framework OpenGL -framework Cocoa -o teste3`

-> Para executar graficamente a parte 1 do projeto (Boucing Ball).

ou

`g++ test-springmass-graphics.cpp graphics.cpp springmass.cpp -framework GLUT -framework OpenGL -framework Cocoa -o teste4`

-> Para executar graficamente a parte 2 do projeto (Spring Mass).

Podemos executar o programa criado utilizando o comando `./teste3` (parte 1) ou `./teste4` (parte 2).

-----------------------------------------------------------------------------

## Diagrama de Sequencia

![Diagrama de Sequencia](SequenceDiagram.jpeg)

-----------------------------------------------------------------------------

## Diagrama de Classes

![Diagrama de Classes](ClassDiagramPart3.jpeg)

-----------------------------------------------------------------------------

## Saída Grafica do Bouncing Ball

![Bouncing Ball](BouncingBall.jpg)

-----------------------------------------------------------------------------

## Saída Grafica do Spring Mass

![Spring Mass](SpringMass.jpg)

-----------------------------------------------------------------------------