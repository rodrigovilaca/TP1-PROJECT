/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{

private:
  Figure figure ;

public:
  SpringMassDrawable()
  : figure("Spring Mass")
  {
    figure.addDrawable(this) ;
  }

  void draw() {
  	int tamM = (int)masses.size() ;
  	int tamS = (int)springs.size() ;
  	for (int i =0;i<tamM;i++){
    	figure.drawCircle(masses[i].getPosition().x,masses[i].getPosition().y,masses[i].getRadius()) ;
	}
	for (int j =0;j<tamS;j++){
    	figure.drawLine(springs[j].getMass1()->getPosition().x,springs[j].getMass1()->getPosition().y,springs[j].getMass2()->getPosition().x,springs[j].getMass2()->getPosition().y,1);
    }
    }	
  

  void display() {
    figure.update() ;
  }

} ;

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;

  SpringMassDrawable springmass ;

  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double stiffness = 1;
  const double damping = 0.01;

  Mass m1(Vector2(-.5,0), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;

  springmass.addMass(m1);
  springmass.addMass(m2);
  springmass.addSpring(0,1,naturalLength,stiffness,damping);

  run(&springmass, 1/120.0) ;
  return 0;
}
