/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"
#include <iostream>
using namespace std;

void run(Simulation & s, double dt) {
    for (int i = 0 ; i < 100 ; ++i) { s.step(dt) ; s.display() ; }
}

int main(int argc, char** argv)
{
  Ball ball ;
  //double px,py;
  const double dt = 1.0/30 ;
  /*for (int i = 0 ; i < 100 ; ++i) {
    ball.step(dt) ;
    ball.display() ;
    //ball.setPosition(0.1,0.2);
    px = ball.getPositionX();
    py = ball.getPositionY();
    cout << "Position: " << px << ","<< py<<endl;
  }*/
    run(ball, dt);
  return 0 ;
}
