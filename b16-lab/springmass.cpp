/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"

#include <iostream>

/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

Mass::Mass()
: position(), velocity(), force(), mass(1), radius(1)
{ }

Mass::Mass(Vector2 position, Vector2 velocity, double mass, double radius)
: position(position), velocity(velocity), force(), mass(mass), radius(radius),
xmin(-1),xmax(1),ymin(-1),ymax(1)
{ }

void Mass::setForce(Vector2 f)
{
  force = f ;
}

void Mass::addForce(Vector2 f)
{
  force = force + f ;
}

Vector2 Mass::getForce() const
{
  return force ;
}

Vector2 Mass::getPosition() const
{
  return position ;
}

Vector2 Mass::getVelocity() const
{
  return velocity ;
}

double Mass::getRadius() const
{
  return radius ;
}

double Mass::getMass() const
{
  return mass ;
}

double Mass::getEnergy(double gravity) const
{
  double energy = 0 ;
  double ec, ep;
  ec = (mass * velocity.norm() * velocity.norm2())/2;
  ep = (mass * gravity * (position.y - ymin - radius));
  energy = ec + ep;
  return energy ;
}

void Mass::step(double dt)
{ 
  Vector2 accel = force / mass ;
  Vector2 position_ = position + dt * velocity + 0.5 * accel * (dt * dt) ;

  if (xmin + radius <= position_.x &&
      position_.x <= xmax - radius) {
    position.x = position_.x ;
    velocity.x = velocity.x + dt * accel.x ;
  } else {
    velocity.x = - velocity.x ;
  }

  if (ymin + radius <= position_.y &&
      position_.y <= ymax - radius) {
    position.y = position_.y ;
    velocity.y = velocity.y + dt * accel.y ;
  } else {
    velocity.y = - velocity.y ;
  }

}

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

Spring::Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping)
: mass1(mass1), mass2(mass2),
naturalLength(naturalLength), stiffness(stiffness), damping(damping)
{ }

Mass * Spring::getMass1() const
{
  return mass1 ;
}

Mass * Spring::getMass2() const
{
  return mass2 ;
}

Vector2 Spring::getForce() const
{
  Vector2 F ;
  Vector2 v1 = mass2->getVelocity() ;
  Vector2 v2 = mass1->getVelocity() ;
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  double length = u.norm() ;
  u = u / u.norm() ;
  double elongationVelocity = dot(v2 - v1, u) ;
  double force = (naturalLength - length) * stiffness + elongationVelocity * damping ;
  F = force * u ;

  return F ;
}

double Spring::getLength() const
{
  Vector2 u = (mass2->getPosition()) -(mass1->getPosition()) ;
  return u.norm() ;
}

double Spring::getEnergy() const {
  double length = getLength() ;
  double dl = length - naturalLength;
  return 0.5 * stiffness * dl * dl ;
}

std::ostream& operator << (std::ostream& os, const Mass& m)
{
  os<<"("
  <<m.getPosition().x<<","
  <<m.getPosition().y<<")" ;
  return os ;
}

std::ostream& operator << (std::ostream& os, const Spring& s)
{
  return os<<"$"<<s.getLength() ;
}

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

SpringMass::SpringMass(double gravity)
: gravity(gravity)
{ }

void SpringMass::display()
{

  int i, j;
  int tamM = (int)masses.size() ;
  int tamS = (int)springs.size() ;

  for (i=0 ; i<tamM ; i++){
    std::cout /*<< "massa: "*/ << masses[i] << " , " ;
  }
  
  for(j=0; j<tamS ; j++){
    std::cout /*<< "mola: "*/ << springs[j] << std::endl ;
  }
  //std::cout << "Energy: " << getEnergy() << std::endl ;

}

double SpringMass::getEnergy() const
{
  double energy = 0 ;
  double massesen = 0;
  double springsen = 0;
  int i, j;
  int tamM = (int)masses.size();
  int tamS = (int)springs.size();

  for (i=0 ; i<tamM ; i++){
    massesen = massesen + masses[i].getEnergy(gravity);
  }
  for(j=0; j<tamS ; j++){
    springsen = springsen + springs[j].getEnergy();
  }

  energy = massesen + springsen;

  return energy ;
}

void SpringMass::step(double dt)
{
  Vector2 g(0,-gravity) ;
  int i, j;
  int tamM = (int)masses.size() ;
  int tamS = (int)springs.size() ;

  for (i=0 ; i<tamM ; i++){
    masses[i].setForce(g * masses[i].getMass());
  }


  for(j=0; j<tamS ; j++){
    Vector2 force = springs[j].getForce();
    springs[j].getMass1()->addForce(-1 * force) ;
    springs[j].getMass2()->addForce(+1 * force) ;
  }

  for (i=0 ; i<tamM ; i++){
    masses[i].step(dt);
  }


}

void SpringMass::addMass(Mass mass1)
{

  masses.push_back(mass1);
}

void SpringMass::addSpring(int i, int j, double naturalLength, double stiffness, double damping)
{
  springs.push_back(Spring(&masses[i],&masses[j],naturalLength,stiffness,damping));
}


